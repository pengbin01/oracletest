# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验注意事项，完成时间： 2023-05-16日前上交

- 请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的Oracle项目中的test5目录中。
- 上交后，通过这个地址应该可以打开你的源码：https://github.com/你的用户名/oracle/tree/master/test5
- 实验分析及结果文档说明书用Markdown格式编写。

## 脚本代码

```sql
create or replace PACKAGE MyPack IS

FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2);
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![1684198455322](1684198455322.png)

## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                           4400
           20 Marketing                              19000
           30 Purchasing                             24900
           40 Human Resources                       6500
           50 Shipping                              156400
           60 IT                                    28800
           70 Public Relations                         10000
           80 Sales                                304500
           90 Executive                              58000
          100 Finance                               51608
          110 Accounting                             20308

DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
          120 Treasury                                   
          130 Corporate Tax                              
          140 Control And Credit                           
          150 Shareholder Services                         
          160 Benefits                                    
          170 Manufacturing                               
          180 Construction                                
          190 Contracting                                 
          200 Operations                                  
          210 IT Support                                 
          220 NOC                                       

DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
          230 IT Helpdesk                                
          240 Government Sales                           
          250 Retail Sales                                 
          260 Recruiting                                   
          270 Payroll                        

过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```

![1684198522131](1684198522131.png)

![1684198536602](1684198536602.png)

![1684198548124](1684198548124.png)

![1684198626232](1684198626232.png)

**实验总结：**

答：经过本次实验，我了解到 PL/SQL语言的特点及功能特性： 与SQL语言紧密集成，所有的SQL语句在PL/SQL中都可以得到支持。 2. 减小网络流量，提高应用程序的运行性能。 3. 模块化的程序设计功能，提高了系统的可靠性。 4. 服务器端程序设计的可移植性好。  PL/SQL程序结构及各个部分的作用：声明部分：以关键字DECLARE开始，BEGIN结束。主要用于声明变量、常量、数据类型、游标、异常处理名称以及本地（局部）子程序定义等。 执行部分：是PL/SQL块的功能实现部分，以关键字BEGIN开始，EXCEPTION或END结束（如果PL/SQL块中没有异常处理部分，则以END结束）。该部分通过变量赋值、流程控制、数据查询、数据操纵、数据定义、事务控制、游标处理等实现块的功能。异常处理部分：以关键字EXCEPTION开始，END结束。该部分用于处理该块执行过程中产生的异常。 
另外Oracle中对运行时错误的处理采用了异常处理机制。一个错误对应一个异常，当错误产生时就抛出相应的异常，并被异常处理器捕获，程序控制权传递给异常处理器，由异常处理器来处理运行时错误。

 