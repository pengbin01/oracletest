CREATE OR REPLACE TRIGGER Stocko
AFTER INSERT ON StockOut
FOR EACH ROW
BEGIN
    IF inserting THEN
        UPDATE StockHold SET Stockcount = StockCount - :new.OutCount WHERE StockHold.ItemID = :new.ItemID;
    END IF;
END;
