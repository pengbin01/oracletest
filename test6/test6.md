

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 |

**班级：软工二班    学号：202010414213    姓名：彭斌** 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 1. 创建基本表

Table: buy

```sql
CREATE TABLE Buy (
   ReceiptID            NUMBER(6)                      DEFAULT 1 NOT NULL,
   ItemID               VARCHAR2(6)                     NOT NULL,
   ItemName             VARCHAR2(50)                    NOT NULL,
   ItemPrice            NUMBER(30)                      NOT NULL,
   SaleNum              NUMBER(4)                       NOT NULL,
   Sum                  NUMBER(8,2)                     NOT NULL,
   BuyDate              DATE                            NOT NULL
)

```

![1685002464135](1685002464135.png)

 Table: Customer    

```sql
CREATE TABLE Customer  (
   ReceiptID            NUMBER(6)                      default 1 not null,
   constraint PK_CUSTOMER primary key (ReceiptID)
)

```

![1685002549187](1685002549187.png)

​        

 Table: Item   

```sql
CREATE TABLE Item  (
   ItemID               VARCHAR2(6)                     not null,
   ItemName             VARCHAR2(50)                    not null,
   ProduceDate          DATE                            not null,
   EndDate              NUMBER                          not null,
   constraint PK_ITEM primary key (ItemID)
)

```

![1685002630695](1685002630695.png)

​        

Table: Provide    

```sql
CREATE TABLE Provide  (
   ProvideID            VARCHAR2(10)                    not null,
   ProvideName          VARCHAR2(50)                    not null,
   ProvideAddress       VARCHAR2(250),
   ProvidePhone         VARCHAR2(25)                    not null,
   constraint PK_PROVIDE primary key (ProvideID)
)

```

![1685002679680](1685002679680.png)

​        

Table: Rack   

```sql
CREATE TABLE Rack  (
   RackID               VARCHAR2(4)                     not null,
   RackAdd              VARCHAR2(6)                     not null,
   RackVol              NUMBER                          not null,
   constraint PK_RACK primary key (RackID)
)

```

![1685002722806](1685002722806.png)

​        

 Table: Sale 

```sql
CREATE TABLE Sale  (
   ItemID               VARCHAR2(6)                     not null,
   ItemName             VARCHAR2(50)                    not null,
   SaleNum              VARCHAR2(50)                    not null,
   SaleDate             DATE                            not null,
   ItemPrice            NUMBER(30),
   constraint PK_SALE primary key (ItemID)
)

```

   

![1685002777358](1685002777358.png)

​        

Index: Index_SaleDate 

   

```sql
CREATE INDEX Index_SaleDate ON Sale(SaleDate ASC);
```

 

![1685002860340](1685002860340.png)

​         Table: Stock  

```sql
CREATE TABLE Stock  (
   StockID              VARCHAR2(4)                     not null,
   StockAdd             VARCHAR2(50)                    not null,
   StockVol             VARCHAR2(50)                    not null,
   constraint PK_STOCK primary key (StockID)
)

```

![1685002899185](1685002899185.png)

​        Table: StockHold    

```SQL
CREATE TABLE StockHold  (
   ItemID               VARCHAR2(6)                     not null,
   StockID              VARCHAR2(4)                     not null,
   StockCount           NUMBER(6)                       not null
      constraint CKC_STOCKCOUNT_STOCKHOL check (StockCount >= 0),
   MinCount             NUMBER(4),
   constraint PK_STOCKHOLD primary key (ItemID, StockID)
)

```

![1685002931173](1685002931173.png)

​        Index: Relationship_4_FK   

```sql
CREATE INDEX Relationship_4_FK on StockHold (
   StockID ASC

```

![1685002970493](1685002970493.png)

​         Index: Relationship_5_FK    

```SQL
CREATE INDEX Relationship_5_FK ON StockHold (
   ItemID ASC
);

```

![1685003020985](1685003020985.png)

​         Table: StockIn      

```sql
CREATE TABLE StockIn  (
   ItemID               VARCHAR2(6)                     not null,
   ItemName             VARCHAR2(50),
   ProvideID            VARCHAR2(10)                    not null,
   ItemPriceIn          NUMBER(30)                      not null,
   InCount              NUMBER(6),
   TotalPrice           NUMBER(8,2),
   InDate               DATE,
   ProduceDate          DATE                            not null,
   EndDate              NUMBER
)

```

![1685003056877](1685003056877.png)

​        Index: Relationship_2_FK    

```sql
CREATE INDEX Relationship_2_FK on StockIn (
   ItemID ASC
)

```

![1685003088486](1685003088486.png)

​         Index: Relationship_3_FK  

  

```sql
CREATE INDEX Relationship_3_FK on StockIn (
   ProvideID ASC
)

```

![1685003117340](1685003117340.png)

​         Index: Index_InDate   

```sql
create index Index_InDate on StockIn (
   InDate ASC
)

```

![1685003156429](1685003156429.png)

​        Table: StockOut   

```sql
CREATE TABLE StockOut  (
   ItemID               VARCHAR2(6)                     not null,
   RackID               VARCHAR2(4)                     not null,
   ItemName             VARCHAR2(50)                    not null,
   OutCount             NUMBER(6)                       not null,
   OutDate              DATE                            not null,
   ItemPrice            NUMBER(30)                      not null,
   Rebate               FLOAT(3)
)

```

![1685003190798](1685003190798.png)

​                 

​        

Index: Relationship_6_FK   

Index: Relationship_7_FK    

Index: Index_OutDate  

 

```sql
CREATE INDEX Relationship_6_FK on StockOut (
   RackID ASC
);

CREATE INDEX Relationship_7_FK on StockOut (
   ItemID ASC
);

create index Index_OutDate on StockOut (
   OutDate ASC
);

```

![1685003286092](1685003286092.png)

Table: "User"

​    

```sql
CREATC TABLE "User"  (
   UserID               VARCHAR2(30)                    not null,
   UserPW               VARCHAR2(50)                    not null,
   UserName             VARCHAR2(30)                    not null,
   UserType             VARCHAR2(20)                    not null,
   constraint PK_USER primary key (UserID)
)

```

##  2. 创建视图



​        View: v_Item   

```sql
CREATE OR REPLACE VIEW v_Item AS
SELECT
   Item.ItemID,
   Item.ItemName AS Item_ItemName,
   StockHold.StockCount,
   StockOut.ItemPrice
FROM
   Item
   JOIN StockHold ON Item.ItemID = StockHold.ItemId
   JOIN StockOut ON Item.ItemID = StockOut.ItemID
WITH READ ONLY;

```

![1685003804644](1685003804644.png)

​        View: v_Provide   

```sql
CREATE OR REPLACE VIEW v_Provide AS
SELECT
   Provide.ProvideName,
   StockIn.ItemName,
   StockIn.ItemPriceIn
FROM
   Provide
   JOIN StockIn ON Provide.ProvideID=StockIn.ProvideID
WITH READ ONLY;

```

![1685003858590](1685003858590.png)

## 3. 创建存储过程-  

```sql
CREATE OR REPLACE PROCEDURE P_Buy
(
p_ItemID IN VARCHAR2,
p_SaleNum IN NUMBER,
p_ItemPrice NUMBER,
p_BuyDate IN DATE DEFAULT SYSDATE
)
AS
p_Sum NUMBER;
p_ReceiptID NUMBER;
p_ItemName VARCHAR2(6);
BEGIN
SELECT * INTO p_ReceiptID FROM Customer;
SELECT ItemName INTO p_ItemName FROM Item WHERE Item.ItemID=p_ItemID;
p_Sum:=p_ItemPrice*p_SaleNum;
INSERT INTO Buy(ReceiptID,ItemID,ItemName,ItemPrice,SaleNum,Sum,BuyDate)
VALUES(p_ReceiptID,p_ItemID,p_ItemName,p_ItemPrice,p_SaleNum,p_Sum,p_BuyDate);
END;

```

```sql
CREATE OR REPLACE PROCEDURE P_StockIn (
    p_ItemID IN VARCHAR2,
    p_ProvideID IN VARCHAR2,
    p_ItemPriceIn IN NUMBER,
    p_InCount IN NUMBER,
    p_EndDate IN NUMBER,
    p_ProduceDate IN DATE,
    p_InDate IN DATE DEFAULT SYSDATE
) AS
    p_TotalPrice NUMBER;
    p_ItemName VARCHAR2(50);
BEGIN
    p_TotalPrice:=p_ItemPriceIn*p_Incount;
    SELECT ItemName INTO p_ItemName FROM Item WHERE Item.ItemID=p_ItemID;
    INSERT INTO StockIn(ItemID,ItemName,ProvideID,ItemPriceIn,InCount,TotalPrice,EndDate,ProduceDate,InDate)
    VALUES(p_ItemID,p_ItemName,p_ProvideID,p_ItemPriceIn,p_InCount,p_TotalPrice,p_EndDate,p_ProduceDate,p_InDate);
END;

```

```sql
CREATE OR REPLACE PROCEDURE P_StockOut (
    p_ItemID IN VARCHAR2,
    p_RackID IN VARCHAR2,
    p_OutCount IN NUMBER,
    p_ItemPrice IN NUMBER,
    p_Rebate IN FLOAT,
    p_OutDate IN DATE DEFAULT SYSDATE
) AS
    count1 NUMBER;
    p_ItemName VARCHAR2(50);
BEGIN
    SELECT StockCount INTO count1 FROM StockHold WHERE StockHold.ItemID=p_ItemID;
    SELECT ItemName INTO p_ItemName FROM Item WHERE Item.ItemID=p_ItemID;
    IF count1>p_OutCount THEN
        INSERT INTO StockOut(ItemID,RackID,ItemName,OutCount,ItemPrice,Rebate,OutDate)
        VALUES(p_ItemID,p_RackID,p_ItemName,p_OutCount,p_ItemPrice,p_Rebate,p_OutDate);
    ELSE 
        dbms_output.put_line('仓库存储量不足'); 
    END IF;
END;

```



![1685003891755](1685003891755.png)



![1685003933505](1685003933505.png)

![1685003954781](1685003954781.png)

##    4.  创建触发器

```sql
CREATE OR REPLACE TRIGGER Buy_Sale 
AFTER INSERT ON Sales
 FOR EACH ROW 
BEGIN 
IF :NEW.Type = 'Buy' THEN 
UPDATE Products 
SET Quantity = Quantity + :NEW.Quantity 
WHERE ProductID = :NEW.ProductID; 
ELSIF :NEW.Type = 'Sale' THEN 
UPDATE Products 
SET Quantity = Quantity - :NEW.Quantity
   WHERE ProductID = :NEW.ProductID;
  END IF;
 END;

```

```sql
CREATE OR REPLACE TRIGGER ItemAdd
AFTER INSERT ON Item
FOR EACH ROW
BEGIN
  IF INSERTING THEN
    INSERT INTO Stockhold(ItemID, StockID, StockCount, MinCount)
    VALUES(:new.ItemID, '1', 0, 20);
    INSERT INTO Sale(ItemID, ItemName, SaleNum, SaleDate, ItemPrice)
    VALUES(:new.ItemID, :new.ItemName, 0, SYSDATE, 0);
  END IF;
END;

```

```sql
CREATE OR REPLACE TRIGGER StockHold_notice
AFTER UPDATE OF ItemID, StockID ON StockHold
FOR EACH ROW
BEGIN
    IF :new.StockCount < :new.MinCount THEN
        dbms_output.put_line('库存较低，请及时进货');
    END IF;
END;

```

```sql
CREATE OR REPLACE TRIGGER Stocki
AFTER INSERT ON StockIn
FOR EACH ROW
BEGIN
    IF inserting THEN
        UPDATE StockHold SET StockCount = StockCount + :new.InCount WHERE StockHold.ItemID = :new.ItemID;
    END IF;
END;

```

```sql
CREATE OR REPLACE TRIGGER Stocko
AFTER INSERT ON StockOut
FOR EACH ROW
BEGIN
    IF inserting THEN
        UPDATE StockHold SET Stockcount = StockCount - :new.OutCount WHERE StockHold.ItemID = :new.ItemID;
    END IF;
END;

```



![1685004084323](1685004084323.png)

![1685004104810](1685004104810.png)

![1685004124883](1685004124883.png)

![1685004143859](1685004143859.png)